#!/usr/bin/env perl

my $debug = 0;
 
package AlgoDownloader;

    use warnings;
    use strict;

    use Data::Dumper;
    use Carp qw( croak );
    
    use Cwd;
    use File::Spec::Functions qw( catfile );
    use File::Path qw( make_path );
    use LWP::UserAgent;
    use HTML::TokeParser::Simple;

    my $URL = 'http://frederic.bapst.home.hefr.ch/algo'; # the partial url, always the same
    my @OVERWRITE_ANSWERS = qw( y yes n no yesall noall ); # the list of possible answers to a confirm prompt
    
    sub new{ # $self ( { year => $year_no, folder => $destination_path } )
        my ( $package, $opts ) = @_;
        # checks that year nbr and destination folder were provided
        croak ( "missing arguments, usage : AlgoDownloader->new( { year => x, folder => y [, tp => z ]  }" )
            unless exists $opts->{ year } and exists $opts->{ folder };
        
        # opts contains all we need excepts the useragent
        $opts->{ ua } = LWP::UserAgent->new;
        $opts->{ write_handler } = \&__write_to_file;
        bless $opts, $package;
        
        # in the end, be sure that the folder is valid by using the proper setter
        $opts->destination_folder( $opts->{ folder } );
        $opts->subdirs_enabled( 1 ); # by default, the .java are put in a subdirectory named after the tp number
        return $opts;
    }
    
    #--------------- public methods --------------------

    sub download_sources{ # void ( $self, $tp_nbr )
        
        my ( $self, $tp_no ) = @_;
        $self->tp_no( $tp_no ) if $tp_no =~ /^\d+$/; # the optional tp nbr arg is supplied 
        
        my $base_url = $self->__sources_url; # creates the full url
        my $ua = $self->{ ua };
        print " -- $base_url\n" if $debug;
        # gets the page
        my $page = $ua->get( $base_url ); 
        print "Sorry, the tp n° ", $self->tp_no, " does not exist\n" and return '' if $page->status_line =~ /404/; 
        # parses the page
        $_ = $page->content;
        my $parser = HTML::TokeParser::Simple->new( \$_ );
        my @as; # the links, or 'a' tags
        
        # parses the page to get the potential links
        while ( my $div = $parser->get_tag( "div" ) ) {
            # loops until it finds div#listing
            next unless $div->get_attr( 'id' ) and $div->get_attr( 'id' ) eq 'listing'; 
                
            # we are inside div#listing, get the links
            while(my $a = $parser->get_tag('a')){ push @as, $a; }
            print Dumper( \@as ) if $debug;
            last; # end while
        } 
        
        
        for my $a (@as){ # iterates throught the potential links
            
            my $href = $a->get_attr( 'href' );
            next if not $href or $href !~ /\.ja(va|r)$/; # we want only java or jar links
            
            # gets the content
            my $resp = $ua->get( "$base_url/$href" ); # downloads the .java | .jar
            $_ = $resp->status_line;
            print "Could not get $href ($_)..." and next unless $_ =~ /200/; # checks the http status
            
             # tries to download the file
            print "Downloaded $href\n" if $self->write_handler->( $self, $href, $resp->content );
      }
    }

    #  - - - - - - -
    
    sub download_enonce{ # void ( $self, $tp_nbr )
        my ( $self, $tp_no ) = @_;
        $self->tp_no( $tp_no ) if $tp_no =~ /^\d+$/; # the optional tp nbr arg is supplied 
        
        my $year = $self->{ year };
        
        # constructs the pdf's name
        $_ = $self->tp_no;
        my $enonce_name = ( int( $_ ) < 10 ? '0' : '' ) . "$_.pdf";
        
        # constructs the url and downloads it
        $_ = "$URL$year/doc/s$enonce_name";
        my $resp = $self->{ ua }->get( $_ );
        
        if( $resp->status_line !~ /200/ ){ # the http status is not ok
            print "Download enoncé $enonce_name failed (" . $resp->status_line . ")\n";
            return '';
        }
        
        # if the status is ok, writes to file
        print "Downloaded $enonce_name\n" if $self->write_handler->( $self, $enonce_name, $resp->content );
        
    }
    
    #--------------- getters/setters --------------------
    
    sub destination_folder{ # $path ( $self [, $new_path ] )
        my ( $self, $path ) = @_;
        
        if( $_ ){ # getter
            return $self->{ folder };
            
        }else{ # setter
            $_ = $path;
            s/(~)/${ENV{HOME}}/g; # replaces ~ by the userhome path
            print " -- raw folder path : $_\n" if $debug;
            $_ = Cwd::realpath( $_ ); 
            croak "The given folder path is invalid...\n" 
                unless -d $_ or make_path $_, { verbose => 1 }; # the file does not exist or could not be created
            
            print "folder : $_\n" if $debug;     
            $self->{ folder } = $_;
        }
    }
    
    #  - - - - - - -
    
    sub force_overwrite{ # $force ( $self [, $force ] )
        my ( $self, $val ) = @_;
        defined $val ? $self->{ overwrite } = 1 : $self->{ overwrite };
    }
    
    #  - - - - - - -
    
    sub subdirs_enabled{
        my ( $self, $val ) = @_;
        $self->{ subdirs } = $val if defined $val;
        return $self->{ subdirs };
    }
    
    #  - - - - - - -
    
    sub tp_no{ # $tp_nbr ( $self [, $new_tp_nbr ] )
        my ( $self, $tp_no ) = @_;
        $tp_no ? $self->{ tp } = $tp_no : $self->{ tp };
    }
    
    #  - - - - - - -
    
    # getter only !
    sub tp_name{ # $tp_nbr ( $self )
        $_ = shift->tp_no; 
        s/^(\d)$/"0$1"/e; 
        return "s$_";
    }
    
    #  - - - - - - -
    
    sub year_no{ # $year_nbr ( $self [, $new_year_nbr ] )
        my ( $self, $year_no ) = @_;
        $year_no ? $self->{ year } = $year_no : $self->{ year };
    }
    
    #  - - - - - - -
    
    sub write_handler{ # $year_nbr ( $self [, $sub ] )
        my ( $self, $write_handler ) = @_;
        
        if( $write_handler ){ # setter
            if( ref $write_handler eq 'CODE' ){ # is a sub reference
                $self->{ write_handler } = $write_handler; 
            }else{ # not a sub
                print "write_handler : not a subroutine reference\n" if $debug;
            }
        }
        # getter
        return $self->{ write_handler };
    }
    
    #--------------- private methods --------------------
    
    sub __sources_url{
        my $self = shift;
        my $year = $self->{ year };
        my $tp_num = $self->{ tp };
       return "$URL$year/s" . ( $tp_num < 10 ? '0' : '' ) . $tp_num;
    }

    #  - - - - - - -
    
    sub __dest_folder_with_tpname{
        my $self = shift;
        $_ = catfile( $self->destination_folder, $self->tp_name );
        make_path $_, { verbose => 1 } or die $! unless -d $_;
        return $_;
    }
    
    #  - - - - - - - 
    
    sub __write_to_file{
        my ( $self, $filename, $content ) = @_;
        $_ = ( $filename =~ /\.java$/ and $self->{ subdirs } ) ? # a .java and the subdirs option is enabled
        $self->__dest_folder_with_tpname : $self->destination_folder;
        my $filepath = catfile( $_, $filename ); # creates the full path
        
        if( -f $filepath ){ # if the file already exists
        
            if( exists $self->{ overwrite } ){ # we already asked 
                if( not $self->{ overwrite } ){ # the reply was noall 
                    print "Skipping $filename...\n";
                    return '' ;
                }
                
            }else{ # asks the user wat to do
                print "The file $filename already exists. Overwrite [y|n|yesall|noall] ? ";
                my $answer;
                chomp( $answer = <STDIN> );
                if( not $answer ~~ \@OVERWRITE_ANSWERS ){ die "Unknown answer\n"; }; # not part of the correct answers
                if( $answer =~ /all/ ){ $self->{ overwrite } = ( $answer =~ /y/ ? 1 : '' ); } # if yesall|noall, updates the force flag
                if( $answer =~ /n/ ){ # the answer is no
                    print "Aborting ...\n";
                    return '' ;
                }
            }
        }
        print " -- writing $filepath\n" if $debug;
        if( $filename =~ /\.java$/ and not $self->subdirs_enabled ){
            $content =~ s/\s*package\s+(\S+);//g ; # subdirs disabled : wipes out the package directive 
            $content =~ s/(\s*import\s+)$1.(\S+)/"$1$2"/ge ; # subdirs disabled : wipes out the package directive 
        }
        # writes to file
        local ( *F ); # F is a typeglob => the handle must be localised to be reused multiple times
        open F, ">", $filepath or die $!;
        print F $content, "\n";
        close F;
        
        print " -- written $filepath...\n" if $debug;
        return 1;
    }

1;

