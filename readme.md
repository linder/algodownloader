AlgoDownloader
==============

This simple program provides an easy way to download sources and "énoncés" from Frederic Bapst's website.

!! Not tested on Windows !!

Features
--------

* ** choose which tp(s) you want**: you can download one, two or more tps at once. 
* ** easily create packages** : by setting the -p option, you can create different subdirectories, each one containing a copy of the sources. 
* ** fix import directives **: if you don't want the default package (sXX) or create multiples packages, this script will fix the import and package directives for you.
* ** download sources, énoncés, or both **: by default, only the sources are downloaded. Use the -e and -a options to download only the énoncés or everything.

Usage
-----

 > algodownloader 4 -d ~/algo -nosubdirs

will download the sources for the s04 tp to ~/algo. No s04/ subdirs will be created.

 > algodownloader -y 1 1-22 -p one,two

will download the source for all the tps of year 1 to the current directory. for each tp, you will have two subdirectories in the form : sXX/one/<sources>, sXX/two/<sources>

 > algodownloader 1 -e 

will download only the énoncé for the s04 tp (year 2 by default) in the current directory.

Install
-------

** Linux **

* Clone this repo and make sure that algodownloader and AlgoDownloader.pm are in the same directory. algodownloader must of course be executable.
* if you don't already have it, install the cpan module HTML::TokeParser::Simple 
 > sudo cpan -i HTML::TokeParser::Simple



** Windows **

* download strawberry perl if perl is not already installed (you can type perl -v in a terminal to verify that the install is ok).
* open a terminal
* install the following missing package by typing  
 > perl -MCPAN -e shell
 > install HTML::TokeParser::Simple * or the name of any other missing package


Use the **-h** option for more informations.
